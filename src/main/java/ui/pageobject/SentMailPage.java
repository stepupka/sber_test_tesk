package ui.pageobject;

import static com.codeborne.selenide.Selenide.$$x;

import com.codeborne.selenide.ElementsCollection;
import lombok.Getter;

public class SentMailPage extends AbstractPage {

    public SentMailPage() {
        super(SentMailPage.class);
    }

    @Getter
    private ElementsCollection subjects = $$x("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']/span");

    public boolean findSubject(String lastSubject) {
        boolean isContains = false;
        if (subjects.texts().contains(lastSubject)) {
            isContains = true;
        }
        return isContains;
    }

}
