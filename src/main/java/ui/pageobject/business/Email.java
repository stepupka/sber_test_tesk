package ui.pageobject.business;

public class Email {
    private String addressee_email;
    private String subject;
    private String email_body;

    public Email(String addressee_email, String subject, String email_body) {
        this.addressee_email = addressee_email;
        this.subject = subject;
        this.email_body = email_body;
    }

    public String getAddressee_email() {
        return addressee_email;
    }

    public String getSubject() {
        return subject;
    }

    public String getEmail_body() {
        return email_body;
    }
}
