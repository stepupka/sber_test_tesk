package ui.pageobject;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

import com.codeborne.selenide.SelenideElement;

public class NewEmailPage extends AbstractPage {
    public NewEmailPage() {
        super(NewEmailPage.class);
    }

    private SelenideElement addressInputField = $x("(//div/div/div[@class = 'composeYabbles'])[1]");
    private SelenideElement subjectInputField = $x("//input[@name = 'subject']");
    private SelenideElement textInputField = $x("//div[@class='cke_contents cke_reset' ]/div[@placeholder='Напишите что-нибудь']");
    private SelenideElement sendButton = $x("//span[contains(text(), 'Отправить')]/ancestor::button");

    public NewEmailPage addressInput(String address) {
        addressInputField.clear();
        addressInputField.setValue(address);
        return this;
    }

    public NewEmailPage subjectInput(String subject) {
        subjectInputField.clear();
        subjectInputField.setValue(subject);
        return this;
    }

    public NewEmailPage textInput(String text) {
        textInputField.clear();
        textInputField.setValue(text);
        return this;
    }

    public MainPage sendButtonClick() {
        sendButton.click();
        return page(MainPage.class);
    }
}
