package ui.pageobject;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

public class LoginPage extends AbstractPage {
    public LoginPage() {
        super(LoginPage.class);
    }

    private SelenideElement loginInputField = $x("//input[@id='passp-field-login']");
    private SelenideElement passWordInputField = $x("//input[@id='passp-field-passwd']");
    @Getter
    private SelenideElement pageTitle = $x("//span[@class='WelcomePage-tagline']");
    private SelenideElement nextButton = $x("//button[@id='passp:sign-in']");
    @Getter
    private SelenideElement errorMessage = $x("//div[contains(text(), 'Такой логин не')]");

    public LoginPage emailInput(String email) {
        loginInputField.shouldBe(Condition.visible).click();
        loginInputField.setValue(email);
        nextButton.click();
        return page(LoginPage.class);
    }

    public LoginPage clearEmailInputField() {
        loginInputField.clear();
        return page(LoginPage.class);
    }

    public MainPage passwordInput(String password) {
        passWordInputField.shouldBe(Condition.visible).click();
        passWordInputField.setValue(password);
        nextButton.click();
        return page(MainPage.class);
    }

}
