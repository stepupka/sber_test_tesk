package ui.pageobject;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.Selenide.sleep;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

public class MainPage extends AbstractPage {
    public MainPage() {
        super(MainPage.class);
    }

    @Getter
    private SelenideElement inboxButton = $x("(//a[@href='#tabs/relevant'])[1]");
    @Getter
    private SelenideElement composeButton = $x("//a[@href='#compose']");
    private SelenideElement sentButton = $x("//a[@href='#sent']");

    public NewEmailPage composeButtonClick() {
        composeButton.click();
        return page(NewEmailPage.class);
    }

    public SentMailPage sentButtonClick() {
        sentButton.click();
        sleep(20000);
        return page(SentMailPage.class);
    }

}
