package ui.pageobject;

public class AbstractPage<T extends AbstractPage<?>> {
    protected T self;

    protected AbstractPage(final Class<T> selfClass) {
        this.self = selfClass.cast(this);
    }
}
