package ui.pageobject;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;


public class AboutPage extends AbstractPage {
    public AboutPage() {
        super(AboutPage.class);
    }

    private SelenideElement loginButton = $x("(//span[contains(text(), 'Войти')]/parent::a)[1]");

    @Step
    public LoginPage clickOnLoginButton() {
        loginButton.shouldBe(visible).click();
        return page(LoginPage.class);
    }
}
