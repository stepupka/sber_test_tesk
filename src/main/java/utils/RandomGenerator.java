package utils;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomGenerator {


    public static String generateSubject(int requiredLength) {
        return RandomStringUtils.random(requiredLength, true, false);
    }

    public static String generateBody(int requiredLength) {
        return RandomStringUtils.random(requiredLength, true, true);
    }
}
