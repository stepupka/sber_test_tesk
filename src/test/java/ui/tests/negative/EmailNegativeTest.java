package ui.tests.negative;

import com.codeborne.selenide.Condition;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ui.pageobject.LoginPage;
import ui.tests.BaseTest;

public class EmailNegativeTest extends BaseTest {
    private LoginPage loginPage;

    @DataProvider
    public Object[][] incorrectEmailData() {
        return new Object[][]{
                {"///mailemail.ru"},
                {"mail@emailru"},
                {"почта@mail.ru"},
                {".mail@mail.ru"},
                {"mail@mail..ru"},
                {"mail@mail."},
        };
    }

    @Test(dataProvider = "incorrectEmailData")
    public void emailValidationCheck(String email) {
        loginPage = aboutPage.clickOnLoginButton();
        loginPage.emailInput(email).getErrorMessage().shouldBe(Condition.visible);
        loginPage.clearEmailInputField();
    }
}
