package ui.tests;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.Selenide.open;
import static utils.Config.baseUrl;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import ui.pageobject.AboutPage;

/**
 * This is base test Java-file
 */

public class BaseTest {

    protected AboutPage aboutPage;

    @BeforeClass(alwaysRun = true, description = "Driver Config Setting")
    public void driverSetUp() {
        Configuration.baseUrl = baseUrl;
        Configuration.browser = "chrome";
        Configuration.timeout = 10000;
        Configuration.driverManagerEnabled = true;
    }

    @BeforeClass(alwaysRun = true, dependsOnMethods = {"driverSetUp"})
    public void openBaseUrl() {
        open(Configuration.baseUrl);
        aboutPage = new AboutPage();
    }


    @AfterClass(alwaysRun = true)
    public void quitDriver() {
        closeWebDriver();
    }
}
