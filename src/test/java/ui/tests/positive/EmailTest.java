package ui.tests.positive;

import static utils.Config.logIn;
import static utils.Config.password;
import static utils.RandomGenerator.generateBody;
import static utils.RandomGenerator.generateSubject;

import com.codeborne.selenide.Condition;
import org.testng.Assert;
import org.testng.annotations.Test;
import ui.pageobject.MainPage;
import ui.pageobject.SentMailPage;
import ui.pageobject.business.Email;
import ui.tests.BaseTest;
import utils.Config;


/**
 * Commit
 */
public class EmailTest extends BaseTest {

    MainPage mainPage = new MainPage();

    @Test(description = "Successful Login check")
    public void successfulLoginToMailBoxCheck() {
        mainPage = aboutPage.clickOnLoginButton()
                .emailInput(logIn)
                .passwordInput(password);
        mainPage.getInboxButton().shouldBe(Condition.visible);
    }

    @Test(description = "Send email checking", dependsOnMethods = {"successfulLoginToMailBoxCheck"})
    public void sendEmailCheck() {
        Email email = new Email(Config.addresseeEmail, generateSubject(7)
                , generateBody( 25));
        mainPage.composeButtonClick().addressInput(email.getAddressee_email()).subjectInput(email.getSubject())
                .textInput(email.getEmail_body()).sendButtonClick();
        SentMailPage sentMailPage = mainPage.sentButtonClick();
        Assert.assertTrue(sentMailPage.findSubject(email.getSubject()), "Email was not found");
    }

}
